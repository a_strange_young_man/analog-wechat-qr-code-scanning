package com.jdzcnc.scan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_1.setOnClickListener(this)
        btn_2.setOnClickListener(this)
        btn_3.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_1 -> {
                val intent = Intent(this, ScanActivity_1::class.java)
                startActivity(intent)
            }

            btn_2 -> {
                val intent = Intent(this, ScanActivity_2::class.java)
                startActivity(intent)
            }

            btn_3 -> {
                val intent = Intent(this, ScanActivity_3::class.java)
                startActivity(intent)
            }
        }
    }
}