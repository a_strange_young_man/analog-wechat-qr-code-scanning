package com.jdzcnc.scan

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator
import com.jdzcnc.scan.scan.Scan_2


class ScanActivity_2 : AppCompatActivity() {
    private var button: Button? = null

    //  Step 1 : 初始化 获取控件 设置监听
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_2)

        //获取测试的控件
        button = findViewById(R.id.button) //点击跳转到扫码活动

        //控件监听
        listenerView()
    }

    private fun listenerView() {

        //  Step 2 :跳转到扫描活动
        button!!.setOnClickListener { //=======设置扫描活动  可根据需求设置以下内容
            val intentIntegrator = IntentIntegrator(this)

            //启动自定义的扫描活动，不设置则启动默认的活动
            intentIntegrator.captureActivity = Scan_2::class.java

            //启动扫描
            intentIntegrator.initiateScan()
        }
    }

    //  Step 3 :处理扫码后返回的结果
    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {

            //==是否扫到内容
            if (result.contents != null) {
                Toast.makeText(this, "扫描结果：" + result.contents, Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "取消扫码", Toast.LENGTH_LONG).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}